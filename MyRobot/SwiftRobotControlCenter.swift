//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        while rightIsClear {
            twoLines()
            if  rightIsBlocked  {
                putAfter()
            }
        }
    }
    func twoLines() {
        putAfter()
        uTurnRight()
        putAfter()
        uTurnLeft()
    }
    func doubleMove() {
        move()
        move()
    }
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
    func uTurnRight() {
        turnRight()
        move()
        turnRight()
        move()
    }
    func uTurnLeft() {
        turnLeft()
        move()
        turnLeft()
        
    }
    func putAfter() {
        put()
        while frontIsClear {
            
            move()
            if frontIsClear {
                move()
                put()
                
            }
        }
    }
}

